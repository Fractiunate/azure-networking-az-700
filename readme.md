# Azure Terraform examples

This repo hosts ```terraform``` infrastructure examples describing the architectures of ```Azure Partner Week``` as well as some kubernetes component examples packed as modules (```aks```, ```azurerm_hdinsight_kafka_cluster_```, ```acr```, ```log-analytics```).


## Project Structure

Terraform projects are seperated as a folder and found in ```terraform/```*. 

Module root folder is ```terraform/```*, all files that relate to a module are placed within their corresponding module folder. 

## Terraform

Terraform requires local cli ```version > 1.1.4.```

### Create a Azure Service Principal and Export TF-Provider Credentials

Create a service principal:
https://registry.terraform.io/providers/hashicorp/azuread/latest/docs/guides/service_principal_client_secret

Export TF-Azurerm-Environments:

```
export ARM_CLIENT_ID=<service-principle-appId>
export ARM_SUBSCRIPTION_ID=<az-subscription-id>
export ARM_TENANT_ID=<service-principle-tenant>
export ARM_CLIENT_SECRET=<service-principal-password>
```
### Create Stack with terrafrom
```
cd terraform/\<subproject\>/
terraform init
terraform plan # opt to see changes
terraform apply --auto-approve
```

### Destroy Stack with terrafrom
```
cd terraform/\<subproject\>/
terraform destroy --auto-approve
```

### Remote State

To enable state handling uncomment code in terraform provider blocks:

```terraform
# Define Terraform provider
terraform {
  backend "azurerm" {
    resource_group_name  = <yourstrorageaccount-resource-group>"
    storage_account_name = "<yourstrorageaccount>"
    container_name       = "terraform-state"
    key                  = "$environment.#infra_level.terraform.tfstate"
  }
}
```

> ```infra_level``` is usualy ```base``` or ```service```. E.g. a project with an App-Service depends on a VNET.
