variable "location" {
  default = "West Europe"
  description   = "Location of the resource group."
}

variable "rg_name" {
  description   = "Resource group name"
}

variable "environment" {
  default = "dev"
}

variable "private_subnet_id" {
    default = ""
}
variable "management_subnet_id" {
    default = ""
    }

variable "public_subnet_id" {
    default = ""
}

variable "VM_NAME" {
  default = "TestVM"
}

variable "VM_ADMIN" {
  default = "azure-user"
}

variable "DEFAULT_SSHKEY" {
  # default = "yourpublicsshkey"
}

variable "vm_public_ip" {
  type = bool
  default = false
}

variable "nginx_vm_size" {
  type = string
  description = "Size (SKU) of the virtual machine to create"
  default = "Standard_B2s"
}

variable "nginx_admin_username" {
  description = "Username for Virtual Machine administrator account"
  type = string
  default = "admin"
}

variable "nginx_admin_password" {
  description = "Password for Virtual Machine administrator account"
  type = string
  default = "testpw"
}

# Ubuntu Linux Variables
variable "ubuntu-publisher" {
  type        = string
  description = "Publisher ID for Ubuntu Linux" 
  # default     = "Canonical" 
  default     = "canonical" 
}
variable "ubuntu-offer" {
  type        = string
  description = "Offer ID for Ubuntu Linux" 
  # default     = "UbuntuServer" 
  default     = "0001-com-ubuntu-server-focal" 
}

variable "sku"{
  default = "20_04-lts-gen2"
}

# Debian Linux Variables
variable "debian-publisher" {
  type        = string
  description = "Publisher ID for Debian Linux" 
  default     = "credativ" 
}
variable "debian-offer" {
  type        = string
  description = "Offer ID for Debian Linux" 
  default     = "Debian" 
}