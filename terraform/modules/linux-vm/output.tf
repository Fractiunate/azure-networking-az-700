output "nginx-pw" {
    value = random_password.nginx-vm-password.result
    sensitive = true
}

output "public_ip_address" {
  value = join("", azurerm_public_ip.nginx-vm-ip.*.ip_address)
}


output "private_ip_address" {
  value = azurerm_linux_virtual_machine.nginx-vm.private_ip_address
}

output "vm_ip_config_name" {
  value = local.ip_config_name
}

output "vm_ip_map" {
  value = tomap({
  "private_ip" = azurerm_network_interface.nginx-nic.private_ip_address
  "nic_id" =  azurerm_network_interface.nginx-nic.id
  "ip_config_name" = local.ip_config_name
})
}