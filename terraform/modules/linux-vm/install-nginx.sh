#! /bin/bash

# sudo touch /etc/systemd/resolved.conf
# sudo bash -c 'cat <<EOT >> /etc/systemd/resolved.conf
# [Resolve]
# DNS=1.1.1.1 8.8.8.8
# FallbackDNS=8.8.4.4
# EOT'

sudo apt-get update
sudo apt install nginx -y


# NGINX_INDEX_PATH="/usr/share/nginx/html/index.html"
NGINX_INDEX_PATH="/var/www/html/index.nginx-debian.html"

cmd='echo "<p>Azure Instance-$(sudo dmidecode | grep UUID | xargs)</p>"'
sudo sed -i "17 i $(eval $cmd)" $NGINX_INDEX_PATH