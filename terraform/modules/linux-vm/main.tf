locals {
  ip_config_name = "nginx-ipconfig-${random_pet.server.id}"
  nic_name = "nginx-nic-${random_pet.server.id}"
}


resource "random_pet" "server" {
  keepers = {
    # Generate a new pet name each time we switch to a new AMI id
    VM_ADMIN       = var.VM_ADMIN
    DEFAULT_SSHKEY = var.DEFAULT_SSHKEY
  }
}

# Bootstrapping Template File
data "template_file" "nginx-vm-cloud-init" {
  template = file("${path.module}/install-nginx.sh")
}

resource "null_resource" "pub_ip_null_depends_on_check" {
  count = var.vm_public_ip ? 1 : 0
}


resource "azurerm_public_ip" "nginx-vm-ip" {
  count = var.vm_public_ip ? 1 : 0
  # depends_on=[azurerm_resource_group.network_stack]
  name                = "nginx-ip-${random_pet.server.id}"
  location            = var.location
  resource_group_name = var.rg_name
  allocation_method   = "Static"
}

# Create Network Card for the VM
resource "azurerm_network_interface" "nginx-nic" {
  name                = local.nic_name
  location            = var.location
  resource_group_name = var.rg_name

  ip_configuration {
    name                          = local.ip_config_name
    subnet_id                     = var.private_subnet_id != "" ? var.private_subnet_id : (var.management_subnet_id != "" ? var.management_subnet_id : var.public_subnet_id)
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = var.vm_public_ip ? azurerm_public_ip.nginx-vm-ip[0].id : null
  }

  tags = {
    Name = local.nic_name
  }

  depends_on          = [null_resource.pub_ip_null_depends_on_check[0]]
}

# Create Nginx VM
resource "azurerm_linux_virtual_machine" "nginx-vm" {
  name                  = "nginx-vm-${random_pet.server.id}"
  location              = var.location
  resource_group_name   = var.rg_name
  network_interface_ids = [azurerm_network_interface.nginx-nic.id]
  size                  = var.nginx_vm_size

  computer_name  = "nginx-vm"
  admin_username = random_pet.server.keepers.VM_ADMIN
  admin_password = random_password.nginx-vm-password.result
  
  depends_on            = [azurerm_network_interface.nginx-nic]

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  # delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  # delete_data_disks_on_termination = true


  source_image_reference {
    publisher = var.ubuntu-publisher
    offer     = var.ubuntu-offer
    sku       = var.sku
    version   = "latest"
  }

  os_disk {
    name                 = "nginx-osdisk-${random_pet.server.id}"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  admin_ssh_key {
    username   = random_pet.server.keepers.VM_ADMIN
    public_key = random_pet.server.keepers.DEFAULT_SSHKEY
  }

  disable_password_authentication = false
  custom_data                     = base64encode(data.template_file.nginx-vm-cloud-init.rendered)
}

resource "random_password" "nginx-vm-password" {
  length           = 16
  special          = true
  override_special = "_%@"
}
