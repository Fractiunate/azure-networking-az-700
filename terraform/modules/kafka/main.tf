resource "random_string" "this" {
  length    = 4
  min_lower = 4
  special   = false
  keepers = {
    KAFA_USERNAME = var.kafa_username
    KAFA_PASSWORD = var.kafa_password
  }
}


resource "azurerm_storage_account" "kafka_storage" {
  name = "hdinsightstor${random_string.this.id }"

  resource_group_name      = var.resource_group_name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = var.account_replication_type
}

resource "azurerm_storage_container" "kafka_storage_container" {
  name = "hdinsight-${random_string.this.id }"

  storage_account_name  = azurerm_storage_account.kafka_storage.name
  container_access_type = "private"
}

resource "azurerm_hdinsight_kafka_cluster" "kafka_cluster" {
  name                = "hdicluster${random_string.this.id }"
  resource_group_name = var.resource_group_name
  location            = var.location
  cluster_version     = "4.0"
  tier                = "Standard"

  #   tls_min_version 
  # encryption_in_transit_enabled 
  # tags 

  component_version {
    kafka = var.kafka_version
  }

  gateway {
    username = random_string.this.keepers.KAFA_USERNAME
    password = random_string.this.keepers.KAFA_PASSWORD
  }

  # rest_proxy {
  # security_group_id - (Required) The Azure Active Directory Security Group ID.
  # security_group_name - (Optional) The Azure Active Directory Security Group name.
  # }

  storage_account {
    storage_container_id = azurerm_storage_container.kafka_storage_container.id
    storage_account_key  = azurerm_storage_account.kafka_storage.primary_access_key
    is_default           = true
  }

  roles {
    head_node {
      vm_size            = "Standard_D3_V2"
      username           = random_string.this.keepers.KAFA_USERNAME
      password           = random_string.this.keepers.KAFA_PASSWORD
      virtual_network_id = var.kafka_vnet_id
      subnet_id          = var.kafka_subnet_id
    }

    worker_node {
      vm_size                  = var.vm_size
      username                 = "${random_string.this.keepers.KAFA_USERNAME}-wn"
      password                 = "${random_string.this.keepers.KAFA_PASSWORD}-wn"
      number_of_disks_per_node = 3
      target_instance_count    = 3
      virtual_network_id       = var.kafka_vnet_id
      subnet_id                = var.kafka_subnet_id
    }

    zookeeper_node {
      vm_size            = "Standard_D3_V2"
      username           = "${random_string.this.keepers.KAFA_USERNAME}-zn"
      password           = "${random_string.this.keepers.KAFA_PASSWORD}-nn"
      virtual_network_id = var.kafka_vnet_id
      subnet_id          = var.kafka_subnet_id
    }
  }
}

