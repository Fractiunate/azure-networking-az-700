variable "resource_group_name" {}
variable "location" {}
variable "kafa_username" {}
variable "kafa_password" {}

variable "kafka_version" {
  default = "2.1"
}

variable "vm_size" {
  default = "Standard_D3_V2"
}

variable "account_replication_type" {
  default = "LRS"
}

variable "kafka_vnet_id" {}
variable "kafka_subnet_id" {}