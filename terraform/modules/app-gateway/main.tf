locals {
  backend_address_pool_name      = "${var.vnet_name}-beap"
  frontend_port_name             = "${var.vnet_name}-feport"
  frontend_ip_configuration_name = "${var.vnet_name}-feip"
  http_setting_name              = "${var.vnet_name}-be-htst"
  listener_name                  = "${var.vnet_name}-httplstn"
  request_routing_rule_name      = "${var.vnet_name}-rqrt"
  redirect_configuration_name    = "${var.vnet_name}-rdrcfg"
}


resource "azurerm_public_ip" "app_gw_public_ip" {
  name                = "app-gw-pub-ip-${var.vnet_name}"
  location            = var.location
  resource_group_name = var.rg_name
  allocation_method   = "Dynamic"
  #   sku = var.lb_sku
}


resource "azurerm_application_gateway" "app-gateway-frontend" {
  name                = "${var.vnet_name}-app-gateway-frontend"
  resource_group_name = var.rg_name
  location            = var.location

  sku {
    name     = "Standard_Small"
    tier     = "Standard"
    capacity = 2
  }

  gateway_ip_configuration {
    name      = "my-gateway-ip-configuration"
    subnet_id = var.app_gateway_subnet_id
  }

  frontend_port {
    name = local.frontend_port_name
    port = 80
  }

  frontend_ip_configuration {
    name                 = local.frontend_ip_configuration_name
    public_ip_address_id = azurerm_public_ip.app_gw_public_ip.id
  }

  backend_address_pool {
    name = local.backend_address_pool_name
  }

  backend_http_settings {
    name                  = local.http_setting_name
    cookie_based_affinity = "Disabled"
    path                  = "/"
    port                  = 80
    protocol              = "Http"
    request_timeout       = 60
  }

  http_listener {
    name                           = local.listener_name
    frontend_ip_configuration_name = local.frontend_ip_configuration_name
    frontend_port_name             = local.frontend_port_name
    protocol                       = "Http"
  }

  request_routing_rule {
    name                       = local.request_routing_rule_name
    rule_type                  = "Basic"
    http_listener_name         = local.listener_name
    backend_address_pool_name  = local.backend_address_pool_name
    backend_http_settings_name = local.http_setting_name
  }
}

resource "azurerm_network_interface_application_gateway_backend_address_pool_association" "vm_binding" {
  count                 = length(var.vm_ip_maps)
  network_interface_id  = lookup(element(var.vm_ip_maps, count.index), "nic_id", "NotFound")
  ip_configuration_name = lookup(element(var.vm_ip_maps, count.index), "ip_config_name", "NotFound")
  # ip_configuration_name   = lookup(var.vm_ip_maps[count.index], "ip_config_name", "NotFound")
  backend_address_pool_id = azurerm_application_gateway.app-gateway-frontend.backend_address_pool[0].id
}
