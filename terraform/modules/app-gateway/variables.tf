variable "location" {
  default     = "West Europe"
  description = "Location of the resource group."
}

variable "rg_name" {
  description = "Resource group name"
}

variable "vnet_name" {}
variable "app_gateway_subnet_id" {}


variable "vm_ip_maps" {
  type = list
}