locals {
    lb_name = "AZLoadBalancer"
    frontend_ip_configuration_name = "PublicIPAddress"
}


# locals {
#   pets = { for p in random_pet.vm_pool : p.prefix => p.id }
# }

locals {
  vm_ips_map = {for s in var.vm_ips: index(var.vm_ips, s) => s}
}

# resource "random_pet" "vm_pool" {
#   count  = length(var.vm_ips)
#   prefix = "${count.index}"

# #   separator = " "
# #   length = "3"
# #   prefix = var.zoo_enabled ? element(var.prefix_list, count.index) : var.default_prefix
# }

resource "azurerm_public_ip" "lb_public_ip" {
  name                = "pub-ip-${local.lb_name}"
  location            = var.location
  resource_group_name = var.rg_name
  allocation_method   = "Static"
  sku = var.lb_sku
}

resource "azurerm_lb" "incoming_lb" {
  name                = local.lb_name
  location            = var.location
  resource_group_name = var.rg_name
  sku = var.lb_sku

  frontend_ip_configuration {
    name                 = local.frontend_ip_configuration_name
    public_ip_address_id = azurerm_public_ip.lb_public_ip.id
  }
}


resource "azurerm_lb_rule" "lb_rule" {
  count                          = var.healthProbe ? 1 : 0
  resource_group_name            = var.rg_name
  loadbalancer_id                = azurerm_lb.incoming_lb.id
  name                           = local.lb_name
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = local.frontend_ip_configuration_name
  backend_address_pool_id        = azurerm_lb_backend_address_pool.lb_backend_address_pool.id
  probe_id                       = azurerm_lb_probe.http_health[0].id
}

resource "azurerm_lb_rule" "lb_rule_no_probe" {
  count                          = var.healthProbe ? 0 : 1
  resource_group_name            = var.rg_name
  loadbalancer_id                = azurerm_lb.incoming_lb.id
  name                           = local.lb_name
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = local.frontend_ip_configuration_name
  backend_address_pool_id        = azurerm_lb_backend_address_pool.lb_backend_address_pool.id
    }


resource "azurerm_lb_probe" "http_health" {
  count               = var.healthProbe ? 1 : 0
  resource_group_name = var.rg_name
  loadbalancer_id     = azurerm_lb.incoming_lb.id
  name                = local.lb_name
  port                = 80
  interval_in_seconds = 10
  number_of_probes    = 3
  protocol            = "Http"
  request_path        = "/"
}


resource "azurerm_lb_backend_address_pool" "lb_backend_address_pool" {
  loadbalancer_id = azurerm_lb.incoming_lb.id
  name            = "${local.lb_name}-bea-pool"
}

resource "azurerm_lb_backend_address_pool_address" "public-vm-pool" {
  for_each = local.vm_ips_map
  name = format("${local.lb_name}-pool-adress-%02d", each.key + 1)
  backend_address_pool_id = azurerm_lb_backend_address_pool.lb_backend_address_pool.id
  virtual_network_id      = var.vnet_id
  ip_address              = each.value
}
