variable "location" {
  default     = "West Europe"
  description = "Location of the resource group."
}

variable "rg_name" {
  description = "Resource group name"
}

variable "healthProbe" {
  default = true
}

variable "vnet_id" {}

# variable "vm_count" {
#   default = 1
# }

variable "vm_ips" {
  type    = list
  default = []
}

variable "lb_sku" {
  type = string
  description = "sku - (Optional) The SKU of the Public IP. Accepted values are Basic and Standard. Defaults to Basic."
  default = "Standard"
}
