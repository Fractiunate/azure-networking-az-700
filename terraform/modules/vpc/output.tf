output "public_subnet_id" {
  value       = azurerm_subnet.vnet_public_subnet_0.id
}

output "app_gw_subnet_id" {
  value       = azurerm_subnet.app_gw_subnet.id
}

output "bastion_ip" {
  value       = join("",azurerm_public_ip.bastion_ip.*.ip_address)
}

output "vnet_id" {
  value       = azurerm_virtual_network.network_stack_vnet0.id
}

output "vnet_name" {
  value       = azurerm_virtual_network.network_stack_vnet0.name
}