locals {
  vnet_name = "vnet0"
}

resource "azurerm_network_security_group" "network_stack_sg" {
  name                = "${local.vnet_name}-network-stack-sg"
  location            = var.location
  resource_group_name = var.rg_name
}

resource "azurerm_virtual_network" "network_stack_vnet0" {
  name                = "${local.vnet_name}-network-stack"
  location            = var.location
  resource_group_name = var.rg_name
  address_space       = var.vpc_cidr

  tags = {
    environment = var.environment,
    location = var.location
  }
}


resource "azurerm_subnet" "vnet_public_subnet_0" {
  name                 = "${local.vnet_name}-public-subnet-0"
  resource_group_name  = var.rg_name
  virtual_network_name = azurerm_virtual_network.network_stack_vnet0.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_subnet" "vnet_private_subnet_0" {
  name                 = "${local.vnet_name}-private-subnet-0"
  resource_group_name  = var.rg_name
  virtual_network_name = azurerm_virtual_network.network_stack_vnet0.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_subnet" "vnet_managed_subnet_0" {
  name                 = "${local.vnet_name}-managed-subnet-0"
  resource_group_name  = var.rg_name
  virtual_network_name = azurerm_virtual_network.network_stack_vnet0.name
  address_prefixes     = ["10.0.3.0/24"]
}

resource "azurerm_subnet" "app_gw_subnet" {
  name                 = "${local.vnet_name}-Application-Gateway-subnet-0"
  resource_group_name  = var.rg_name
  virtual_network_name = azurerm_virtual_network.network_stack_vnet0.name
  address_prefixes     = ["10.0.4.0/24"]
}

resource "azurerm_subnet" "AzureBastionSubnet" {
  count = var.create_bastion ? 1 : 0
  name                 = "AzureBastionSubnet"
  resource_group_name  = var.rg_name
  virtual_network_name = azurerm_virtual_network.network_stack_vnet0.name
  address_prefixes     = ["10.0.5.0/24"]
}


resource "azurerm_public_ip" "bastion_ip" {
  count = var.create_bastion ? 1 : 0
  name                = "${local.vnet_name}-bastion-ip"
  location            = var.location
  resource_group_name = var.rg_name
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_bastion_host" "vnet_bastion" {
  count = var.create_bastion ? 1 : 0
  name                = "${local.vnet_name}-bastion"
  location            = var.location
  resource_group_name = var.rg_name

  ip_configuration {
    name                 = "configuration"
    subnet_id            = azurerm_subnet.AzureBastionSubnet[0].id
    public_ip_address_id = azurerm_public_ip.bastion_ip[0].id
  }
}