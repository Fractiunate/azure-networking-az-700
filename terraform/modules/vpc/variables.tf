variable "environment" {
  default = "dev"
}

variable "location" {
  default = "West Europe"
  description   = "Location of the resource group."
}

variable "rg_name" {
  description   = "Resource group name"
}

variable "create_bastion" {
  default = true
}

variable "vpc_cidr" {
  default = ["10.0.0.0/16"]
}