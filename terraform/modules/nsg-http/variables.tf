variable "location" {
  default     = "West Europe"
  description = "Location of the resource group."
}

variable "rg_name" {
  description = "Resource group name"
}

variable "subnet_ids" {
  type = list(string)
}