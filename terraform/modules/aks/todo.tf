# TODO
# var.client_app_id: This variable refers to the client app ID of the Azure AD client application which was mentioned in the prerequisites section.
# var.server_app_id: This variable refers to the server app ID of the Azure AD server application which was mentioned in the prerequisites section.
# var.server_app_secret: This variable refers to the secret created for the Azure AD server application.
# var.tenant_id: This variable refers to the Azure AD tenant ID associated with the subscription where the cluster will be deployed. This value can be obtained from the Azure portal or through the Azure CLI.


# TODO https://www.danielstechblog.io/terraform-deploy-an-aks-cluster-using-managed-identity-and-managed-azure-ad-integration/








# resource "azurerm_kubernetes_cluster" "cluster" {
#   name                = "${var.prefix}-aks"
#   location            = azurerm_resource_group.rg.location
#   resource_group_name = azurerm_resource_group.rg.name
#   dns_prefix          = "${var.prefix}-aks"
#   # kubernetes_version = "1.21"


#   default_node_pool {
#     name                = "default"
#     node_count          = 2
#     vm_size             = "Standard_D2_v2"
#     type                = "VirtualMachineScaleSets"
#     availability_zones  = ["1", "2"]
#     enable_auto_scaling = true
#     min_count           = 2
#     max_count           = 4

#     # Required for advanced networking
#     vnet_subnet_id = azurerm_subnet.cluster_subnet.id
#   }

#   identity {
#     type = "SystemAssigned"
#   }

#   addon_profile {
#     http_application_routing {
#       enabled                    = var.addons.http_application_routing
#     }
#     oms_agent {
#       enabled                    = var.addons.oms_agent
#       # log_analytics_workspace_id = var.log_analytics_workspace_id
#     }
#     kube_dashboard {
#       enabled = var.addons.kubernetes_dashboard
#     }
#     azure_policy {
#       enabled = var.addons.azure_policy
#     }
#   }


#   #   role_based_access_control {
#   #     azure_active_directory {
#   #       client_app_id     = var.client_app_id
#   #       server_app_id     = var.server_app_id
#   #       server_app_secret = var.server_app_secret
#   #       tenant_id         = var.tenant_id
#   #     }
#   #     enabled = true
#   #   }
#   role_based_access_control {
#     enabled = true
#     azure_active_directory {
#       managed = true
#       admin_group_object_ids = [
#         data.azuread_group.aks.id
#       ]
#     }
#   }

#   network_profile {
#     network_plugin    = "azure"
#     load_balancer_sku = "standard"
#     network_policy    = "calico"
#   }

#   tags = {
#     Environment = "Development"
#   }
# }


# # 
# resource "azurerm_kubernetes_cluster_node_pool" "mem" {
#  kubernetes_cluster_id = azurerm_kubernetes_cluster.cluster.id
#  name                  = "mem"
#  node_count            = "1"
#  vm_size               = "standard_d11_v2"
# }


# # 
# variable "client_app_id" {
#   description = "The Client app ID of the AKS client application"
# }

# variable "server_app_id" {
#   description = "The Server app ID of  the AKS server application"
# }

# variable "server_app_secret" {
#   description = "The secret created for AKS server application"
# }

# variable "tenant_id" {
#   description = "The Azure AD tenant id "
# }
