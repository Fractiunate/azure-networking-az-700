####################
# Provider section #
####################
provider "azurerm" {
  features {}
}
#####################
# Resources section #
#####################
resource "azurerm_container_registry" "acr" {
  name                     = var.name
  location                 = var.location
  resource_group_name      = var.resource_group_name
  sku                      = var.sku
  admin_enabled            = var.admin

 
  dynamic "georeplications" {
    for_each = var.geo_replication
    content {
      location                = georeplications.value
      zone_redundancy_enabled = true
      tags                    = {}
    }
  }


}
