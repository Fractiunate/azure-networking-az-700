resource "random_string" "this" {
  length    = 4
  min_lower = 4
  special   = false
  #   keepers = {
  #     RESOURCE_GROUP_NAME = var.resource_group_name
  #     LOCATION            = var.location
  #   }
}

locals {
  storage_acc_name  = "pvstorage${random_string.this.result}"
  setup_secret_path = "/tmp/setup_secret.sh"
}

data "template_file" "setup_secret" {
  template = file("${path.module}/setup_secret.sh")
  vars = {
    AKS_PERS_RESOURCE_GROUP       = var.resource_group_name
    AKS_PERS_STORAGE_ACCOUNT_NAME = local.storage_acc_name
    AKS_CLUSTER_NAME              = var.aks_cluster_name
    SECRET_NAME = "azure-secret"
  }
}

resource "local_file" "setup_secret" {
  content  = data.template_file.setup_secret.rendered
  filename = local.setup_secret_path
}

resource "azurerm_storage_account" "pv_storage_account" {
  name                = local.storage_acc_name
  resource_group_name = var.resource_group_name

  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  # network_rules {
  #   default_action = "Allow"
  #   # ip_rules                   = ["100.0.0.1"]
  #   virtual_network_subnet_ids = var.virtual_network_subnet_ids
  # }

  tags = {
    environment = "staging"
  }

}



resource "azurerm_storage_container" "pv_storage_container" {
  name                  = "vhds${random_string.this.result}"
  storage_account_name  = azurerm_storage_account.pv_storage_account.name
  container_access_type = "private"
  depends_on = [
    azurerm_storage_account.pv_storage_account
  ]
}


resource "azurerm_storage_share" "kube_share" {
  name                 = "sharename${random_string.this.result}"
  storage_account_name = azurerm_storage_account.pv_storage_account.name
  quota                = 50
  depends_on = [
    azurerm_storage_account.pv_storage_account
  ]

}


resource "null_resource" "storage_account_secret" {
  depends_on = [
    azurerm_storage_account.pv_storage_account
  ]
  triggers = {
    file = "${data.template_file.setup_secret.rendered}"
  }
  provisioner "local-exec" {
    command = "/bin/bash ${local.setup_secret_path}"
  }
}

variable "resource_group_name" {}
variable "aks_cluster_name" {}
variable "location" {}
variable "virtual_network_subnet_ids" {
  type = list(string)
}
