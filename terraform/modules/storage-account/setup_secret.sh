#!/bin/bash
STORAGE_KEY=$(az storage account keys list --resource-group ${AKS_PERS_RESOURCE_GROUP} --account-name ${AKS_PERS_STORAGE_ACCOUNT_NAME} --query "[0].value" -o tsv)
az aks get-credentials --resource-group ${AKS_PERS_RESOURCE_GROUP} --name ${AKS_CLUSTER_NAME} --admin --overwrite-existing

if kubectl get secret ${SECRET_NAME} ; then
    kubectl delete secret ${SECRET_NAME} --ignore-not-found
    kubectl create secret generic ${SECRET_NAME} --from-literal=azurestorageaccountname=${AKS_PERS_STORAGE_ACCOUNT_NAME} --from-literal=azurestorageaccountkey=$STORAGE_KEY
else
    kubectl create secret generic ${SECRET_NAME} --from-literal=azurestorageaccountname=${AKS_PERS_STORAGE_ACCOUNT_NAME} --from-literal=azurestorageaccountkey=$STORAGE_KEY
fi

