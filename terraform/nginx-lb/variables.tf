variable "environment" {
  default = "dev"
}

variable "location" {
  default = "West Europe"
  description   = "Location of the resource group."
}

variable "VM_SSH_KEY" {
  
}