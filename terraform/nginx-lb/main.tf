resource "azurerm_resource_group" "network_stack" {
  name     = "network-stack-0"
  location = var.location
}


module "vnet0" {
  source         = "../modules/vpc"
  environment    = var.environment
  location       = azurerm_resource_group.network_stack.location
  rg_name        = azurerm_resource_group.network_stack.name
  create_bastion = false
}


module "nsg-http" {
  source     = "../modules/nsg-http"
  location   = azurerm_resource_group.network_stack.location
  rg_name    = azurerm_resource_group.network_stack.name
  subnet_ids = [module.vnet0.public_subnet_id]
  depends_on = [
    module.vnet0.vnet0,
    module.vnet0.vnet0_public_subnet_0
  ]
}

module "linux_vms" {
  source           = "../modules/linux-vm"
  count            = 3
  environment      = var.environment
  location         = azurerm_resource_group.network_stack.location
  rg_name          = azurerm_resource_group.network_stack.name
  public_subnet_id = module.vnet0.public_subnet_id
  DEFAULT_SSHKEY   = var.VM_SSH_KEY
  depends_on = [
    module.vnet0.vnet0,
    module.vnet0.vnet0_public_subnet_0
  ]
}

# module "load-balancer" {
#   source   = "../modules/load-balancer"
#   location = azurerm_resource_group.network_stack.location
#   rg_name  = azurerm_resource_group.network_stack.name
#   vnet_id  = module.vnet0.vnet_id
#   vm_ips   = [module.linux_vms.private_ip_address, module.linux_vm_2.private_ip_address]
#   depends_on = [
#     module.vnet0.vnet0,
#     module.linux_vm_1.nginx-vm-ip
#   ]
# }

module "app-gw" {
  source                = "../modules/app-gateway"
  location              = azurerm_resource_group.network_stack.location
  rg_name               = azurerm_resource_group.network_stack.name
  vnet_name             = module.vnet0.vnet_name
  app_gateway_subnet_id = module.vnet0.app_gw_subnet_id
  vm_ip_maps            = module.linux_vms[*].vm_ip_map

  depends_on = [
    module.vnet0,
    module.linux_vms
  ]
}
