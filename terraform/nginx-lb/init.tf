# Define Terraform provider
terraform {
  # backend "azurerm" {
  #   resource_group_name  = "Terraform-Resources"
  #   storage_account_name = "az700terraform"
  #   container_name       = "terraform-state"
  #   key                  = "dev.base.terraform.tfstate"
  # }
}


#Configure the Azure Provider
provider "azurerm" {
  features {}

#   environment = var.environment
#   endpoint             = "https://management.myazurestack.fqdn" 

#   subscription_id = "b62bf070-2655-4356-8091-7177732b86c4" #var.azure_subscription_id
#   client_id = var.azure_client_id
#   client_secret = var.azure_client_secret
#   tenant_id = var.azure_tenant_id
}




# # Azure authentication variables
# variable "azure_subscription_id" {
#   type = string
#   description = "Azure Subscription ID"
# }

# variable "azure_client_id" {
#   type = string
#   description = "Azure Client ID"
# }

# variable "azure_client_secret" {
#   type = string
#   description = "Azure Client Secret"
# }

# variable "azure_tenant_id" {
#   type = string
#   description = "Azure Tenant ID"
# }

# data "terraform_remote_state" "tf-state" {
#   backend = "azurerm"
#   config = {
#     resource_group_name  = "Terraform-Resources"
#     storage_account_name = "az700terraform"
#     container_name       = "terraform-state"
#     key                  = "dev.base.terraform.tfstate"
#   }
# }