# output "lb_public_ip" {
#   value = module.load-balancer.lb_public_ip
# }

output "bastion_ip" {
  value = module.vnet0.bastion_ip
}

output "vm_ip_maps" {
  value = [module.linux_vms.*.vm_ip_map]
}
