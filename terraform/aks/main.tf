# & AKS Cluster - David Rahäuser 2022

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.93.1"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "~> 2.0.1"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  name     = "${var.prefix}-rg"
  location = var.location
}

# ----VNET-NETWORKS---------------------------------------------------------------------------

resource "azurerm_virtual_network" "vnet" {
  name                = "${var.prefix}-network"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = ["10.1.0.0/16"]
}

resource "azurerm_virtual_network" "kafka_vnet" {
  name                = "${var.prefix}-kafka-network"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = ["10.10.0.0/24"]
}

resource "azurerm_subnet" "kafka_subnet" {
  name                 = "${var.prefix}-subnet"
  virtual_network_name = azurerm_virtual_network.kafka_vnet.name
  resource_group_name  = azurerm_resource_group.rg.name
  address_prefixes     = ["10.10.0.0/26"]
}


resource "azurerm_subnet" "cluster_subnet" {
  name                 = "${var.prefix}-subnet"
  virtual_network_name = azurerm_virtual_network.vnet.name
  resource_group_name  = azurerm_resource_group.rg.name
  address_prefixes     = ["10.1.0.0/22"]
}

# ----ACR-CONTAINER-REGISTRY------------------------------------------------------------------

module "acr" {
  source              = "../modules/acr"
  name                = "tf${var.prefix}acr4akslocal"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  sku                 = "Premium"
  admin               = false
  geo_replication     = ["France Central", "North Europe"]
}

# ----LOG-ANALYTICS---------------------------------------------------------------------------

module "log_analytics" {
  source              = "../modules/log_analytics"
  name                = "${var.prefix}-logs"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  sku                 = "PerGB2018"
  retention           = 30
}


# ----KAFKA-HDINSIGHTS------------------------------------------------------------------------

module "kafka" {
  count               = var.create_kafka ? 1 : 0
  source              = "../modules/kafka"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  kafka_vnet_id       = azurerm_virtual_network.kafka_vnet.id
  kafka_subnet_id     = azurerm_subnet.kafka_subnet.id
  kafa_username       = "acctestusrgw"
  kafa_password       = "TerrAform123!"
  depends_on          = [azurerm_subnet.kafka_subnet]
}


# ----AKS-CLUSTER-----------------------------------------------------------------------------

module "aks" {
  count                      = var.create_aks ? 1 : 0
  source                     = "../modules/aks"
  resource_group_name        = azurerm_resource_group.rg.name
  location                   = azurerm_resource_group.rg.location
  container_registry_id      = module.acr.id
  log_analytics_workspace_id = module.log_analytics.workspace_id
  name                       = "${var.prefix}-aks"
  kubernetes_version         = "1.22.4"
  private_cluster            = false
  sla_sku                    = "Free"
  vnet_subnet_id             = azurerm_subnet.cluster_subnet.id
  aad_group_name             = "AKS-Admins"
  api_auth_ips               = []
  addons = {
    oms_agent                = true
    kubernetes_dashboard     = false
    azure_policy             = false
    http_application_routing = true
  }
  default_node_pool = {
    name       = "nodepool1"
    node_count = 2
    vm_size    = "Standard_D4_v3"
    # zones                          = ["1", "2", "3"]
    zones                          = ["1", "2"]
    taints                         = null
    cluster_auto_scaling           = false
    cluster_auto_scaling_min_count = null
    cluster_auto_scaling_max_count = null
    labels = {
      "environment" = "demo"
    }
  }
  additional_node_pools = {}
}

module "pv_storage" {
  count                      = var.create_aks ? 1 : 0
  source                     = "../modules/storage-account"
  resource_group_name        = azurerm_resource_group.rg.name
  location                   = azurerm_resource_group.rg.location
  virtual_network_subnet_ids = [azurerm_subnet.cluster_subnet.id]
  aks_cluster_name           = module.aks[0].name
  depends_on = [
    local_file.kubeconfig
  ]
}


# ----KUBE-CONFIG-FILE------------------------------------------------------------------------

resource "local_file" "kubeconfig" {
  count      = var.create_aks ? 1 : 0
  depends_on = [module.aks]
  filename   = "kubeconfig"
  content    = module.aks[0].kube_config_raw
}
