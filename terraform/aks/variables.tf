variable "prefix" {
  description = "A prefix used for all resources in this example"
  default     = "dev"
}

variable "location" {
  default     = "West Europe"
  description = "The Azure Region in which all resources in this example should be provisioned"
}


variable "addons" {
  description = "Defines which addons will be activated."
  type = object({
    oms_agent                = bool
    kubernetes_dashboard     = bool
    azure_policy             = bool
    http_application_routing = bool
  })
  default = {
    azure_policy             = false
    http_application_routing = true
    kubernetes_dashboard     = true
    oms_agent                = false
  }
}

variable "create_aks" {
  default = true
}

variable "create_kafka" {
  default = false
}
