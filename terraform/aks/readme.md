```
export TF_VAR_prefix=<Environment prefix>
export TF_VAR_client_app_id=<The client app ID of the AKS client application>  
export TF_VAR_server_app_id=<The server app ID of the AKS server application>
export TF_VAR_server_app_secret=<The secret created for AKS server application>
export TF_VAR_tenant_id=<The Azure AD tenant id>
```

## Fix Gitbash AZ Windows
export MSYS_NO_PATHCONV=1

# Create a Service Principal for TF credentials
create the Service Principal with:

```bash
az ad sp create-for-rbac \
  --role="Contributor" \
  --scopes="/subscriptions/SUBSCRIPTION_ID"
```
...and allow it to read AD: https://gaunacode.com/azure-ad-permissions-to-read-service-principals

## Retrieve the admin kubeconfig using the Azure cli:
az aks get-credentials --resource-group $prefix-rg --name $prefix-aks --admin --overwrite-existing

## Run the following command to list the nodes and availability zone configuration:

```
kubectl describe nodes | grep -e "Name:" -e "failure-domain.beta.kubernetes.io/zone"

Name:               aks-default-36042037-vmss000000
                    failure-domain.beta.kubernetes.io/zone=westeurope-1
Name:               aks-default-36042037-vmss000001
                    failure-domain.beta.kubernetes.io/zone=westeurope-2

```



# Configure the Azure Active Directory integration
 https://codersociety.com/de/blog/articles/terraform-azure-kubernetes


```
GROUP_ID=$(az ad group create --display-name dev --mail-nickname dev --query objectId -o tsv)

AKS_ID=$(az aks show \
    --resource-group $prefix-rg \
    --name $prefix-aks \
    --query id -o tsv)

# Create an Azure role assignment so that any member of the dev group can use kubectl to interact with the Kubernetes cluster.
az role assignment create \
  --assignee $GROUP_ID \
  --role "Azure Kubernetes Service Cluster User Role" \
  --scope $AKS_ID

# Add yourself to the dev AD group.
USER_ID=$(az ad signed-in-user show --query objectId -o tsv)
az ad group member add --group dev --member-id $USER_ID

```


## Work with generated Kubeconfig
```
kubectl get node --kubeconfig kubeconfig
or 
export KUBECONFIG="${PWD}/kubeconfig"
```
Default:
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/config